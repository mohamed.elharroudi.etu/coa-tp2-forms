#ifndef __SHAREGROUP_H__
#define __SHAREGROUP_H__

#include <vector>
#include "shape.hpp"


class Sharegroup : public Shape {
    
public:
    vector<Shape *> all_shapes;
    Sharegroup(vector<Shape*> shapes);
    virtual void draw(Image &img);
    virtual bool is_inside(int x, int y) const;
    friend ostream& operator<<(ostream& os, const Sharegroup& dt);

#endif

};