#include <cmath>
#include "sharegroup.hpp"


Sharegroup::Sharegroup(vector<Shape *> all_shapes) : Shape(0,0) ,all_shapes(all_shapes) {}

    void Sharegroup::draw(Image &img){
        for (auto shape : this->all_shapes){
            shape->draw(img);
        }
    }


    bool Sharegroup::is_inside(int x, int y) const{
        for (auto shape : this->all_shapes){
            if (shape->is_inside(x,y)){
                return True;
            }
        }
        return False;
    }


    ostream& operator<<(ostream& os, const Sharegroup& dt)
    {
        for(auto shape : dt.all_shapes){
            os << shape << '\n';
        }
        return os;
    }